import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubAboutPage } from './sub-about';

@NgModule({
  declarations: [
    SubAboutPage,
  ],
  imports: [
    IonicPageModule.forChild(SubAboutPage),
  ],
})
export class SubAboutPageModule {}
