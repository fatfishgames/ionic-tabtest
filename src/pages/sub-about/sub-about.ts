import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-sub-about',
  templateUrl: 'sub-about.html',
})
export class SubAboutPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubAboutPage');
  }

  ClosePage()
  {
    this.navCtrl.push('MainTabsPage');
  }
}
