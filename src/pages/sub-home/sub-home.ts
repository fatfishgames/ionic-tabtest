import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-sub-home',
  templateUrl: 'sub-home.html',
})
export class SubHomePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubHomePage');
  }

  ClosePage()
  {
    this.navCtrl.push('MainTabsPage');
  }
}
