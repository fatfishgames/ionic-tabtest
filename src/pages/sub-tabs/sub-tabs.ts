import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage({
  segment: 'tabs'
})
@Component({
  selector: 'page-sub-tabs',
  templateUrl: 'sub-tabs.html',
})
export class SubTabsPage {

  tab1 = 'SubHomePage';
  tab2 = 'SubAboutPage';

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubTabsPage');
  }

}
